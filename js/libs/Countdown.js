class CountDown {
   setCountDown(raceDate, raceUiId) {
        setInterval(function () {
            var now = new Date().getTime();
            var distance = raceDate - now;
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            $("#" +raceUiId).html(hours + "h " + minutes + "m " + seconds + "s ");

            if (distance < 0) {
                document.location.reload();
            }
        }, 1000);
    }
}