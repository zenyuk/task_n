class Race {
    constructor(number, name, startTime, channel, raceType) {
        this.number = number;
        this.name = name;
        this.startTime = new Date(startTime);
        this.channel = channel;
        this.raceType = raceType;
    }
}