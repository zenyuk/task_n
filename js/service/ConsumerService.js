class ConsumerService {
    getRaces(view, limit) {
        $.ajax({
            type: 'GET',
            headers: { 'Access-Control-Allow-Origin': '*' },
            crossDomain: true,
            url: '/race/getAll',
            contentType: "application/json",
            dataType: "json",
            success: function(responseObject) {
                var races = [];
                var now = new Date().getTime();
                responseObject.forEach(function(responseItem){
                    var race = new Race(
                        responseItem.number,
                        responseItem.name,
                        new Date(responseItem.startTime),
                        responseItem.channel,
                        responseItem.raceType);

                    if (race.startTime > now) {
                        races.push(race);
                    }
                });

                races.sort(function (a, b) {
                    return a.startTime - b.startTime;
                });

                view.updateUi(races.slice(0, limit));
            }
        });
    }
}

