var currentView;
if (document.location.href.includes("grouped.html")) {
    currentView = new GroupedView();
} else {
    currentView = new MixedView();
}

$(document).ready(function() {

    currentView.getRaces();

    $("#showMixed").click(function () {
        document.location.href = "index.html";
    });

    $("#showGrouped").click(function () {
        document.location.href = "grouped.html";
    });
});






