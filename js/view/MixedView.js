class MixedView {
    constructor() {
        this.requestIsInProgress = false;
        this.consumerService = new ConsumerService();
        this.countDown = new CountDown();
    }

    makeRaceItem(race) {
        return "<tr ><td>" + race.number + "</td>" +
            "<td>" + race.name + "</td>" +
            "<td id='race"+race.number + "'>" + race.startTime + "</td>" +
            "<td>" + race.channel + "</td></tr>";
    }

    loaded() {
	//		
    }

    getRaces() {
        this.consumerService.getRaces(this, 5);
    }

    updateUi(races) {
        var that = this;

        races.forEach(function (race) {
            var html = that.makeRaceItem(race);
            $('#racesView').append(html);
            that.countDown.setCountDown(race.startTime, "race"+race.number);
        });

        $('#loading').hide();
    }
}

