class GroupedView {
    constructor() {
        this.requestIsInProgress = false;
        this.consumerService = new ConsumerService();
        this.countDown = new CountDown();
    }

    makeRaceItem(race, raceType) {
        return "<tr rt='" + raceType +"'><td>" + race.number + "</td>" +
            "<td>" + race.name + "</td>" +
            "<td id='race"+race.number + "'>" + race.startTime + "</td>" +
            "<td>" + race.channel + "</td></tr>";
    }

    loaded() {
	//		
    }

    getRaces() {
        this.consumerService.getRaces(this, 15);
    }

    updateUi(races) {
        var thoroughbredCount = 0;
        var greyhoundsCount = 0;
        var harnessCount = 0;

        var that = this;
        races.forEach(function (race) {
            var html = "";

            if (race.raceType == "thoroughbred" && thoroughbredCount < 5) {
                html = that.makeRaceItem(race, "thoroughbred");
                $('#thoroughbredView').append(html);
                that.countDown.setCountDown(race.startTime, "race"+race.number);
                thoroughbredCount++;
            } else if (race.raceType == "greyhounds" && greyhoundsCount < 5) {
                html = that.makeRaceItem(race, "greyhounds");
                $('#greyhoundsView').append(html);
                that.countDown.setCountDown(race.startTime, "race"+race.number);
                greyhoundsCount++;
            } else if (race.raceType == "harness" && harnessCount < 5) {
                html = that.makeRaceItem(race, "harness");
                $('#harnessView').append(html);
                that.countDown.setCountDown(race.startTime, "race"+race.number);
                harnessCount++;
            }



        });

        $('#loading').hide();
    }
}

